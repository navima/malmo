from __future__ import print_function

from builtins import range
import MalmoPython
import json
import math
import os
import random
import sys
import time
import errno
from timeit import default_timer as timer
import malmoutils

malmoutils.fix_print()

agent_host = MalmoPython.AgentHost()
malmoutils.parse_command_line(agent_host)

# Test that AbsoluteMovementCommand teleportation works for long distances.

def startMission(agent_host, xml):
	my_mission = MalmoPython.MissionSpec(xml, True)
	my_mission_record = malmoutils.get_default_recording_object(
		agent_host, "teleport_results")
	max_retries = 3
	for retry in range(max_retries):
		try:
			agent_host.startMission(my_mission, my_mission_record)
			break
		except RuntimeError as e:
			if retry == max_retries - 1:
				print("Error starting mission", e)
				print("Is the game running?")
				exit(1)
			else:
				time.sleep(2)

	world_state = agent_host.peekWorldState()
	while not world_state.has_mission_begun:
		time.sleep(0.1)
		world_state = agent_host.peekWorldState()
		for error in world_state.errors:
			print("Error:", error.text)
		if len(world_state.errors) > 0:
			exit(1)


# -- set up the mission -- #
missionXML_file = 'nb4tf4i-teleport.xml'
with open(missionXML_file, 'r') as f:
	print("Loading mission from %s" % missionXML_file)
	worldXML = f.read()
	#my_mission = MalmoPython.MissionSpec(mission_xml, True)
	#my_mission.drawBlock( 0, 0, 0, "lava")


startMission(agent_host, worldXML)
world_state = agent_host.peekWorldState()

# This test can get stuck, since Minecraft sometimes won't redraw the scene unless the agent moves slightly,
# and we don't move until Minecraft redraws the scene.
# To get around this, set a gentle rotation:
agent_host.sendCommand("turn 0.01")

# Teleport to each location in turn, see if we collect the right number of emeralds,
# and check we get the right image for each location.
total_reward = 0

xs = [2, 6, -3, -6, -11, 1, -9, -18, 4, -22, 4, 26, 28, 14, 17, 34, -35, 38, 8, 32, -43, -28, 30, -27, 52, -8, 39, 58, 44, -62,
	  23, 66, 47, -49, -64, 74, 3, 78, 80, -15, 49, -63, -32, -90, 3, -39, 57, 15, 18, 48, -86, -106, -17, 31, -112, -42, 116, -9]
ys = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
	  32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60]
zs = [4, 4, 8, -10, 12, 14, 16, -13, 20, -22, -24, -15, 24, 30, -32, 24, 36, -1, -40, -42, -44, 46, -48, 50, 42, -54, 56, -11, 60, -
	  15, -64, -63, 68, -70, -72, 5, 76, -10, 0, 82, 84, 86, -88, 72, 92, -94, 96, 98, 100, -102, 104, 93, 108, -110, -89, 114, 87, -118]

xs.reverse()
ys.reverse()
zs.reverse()

# xs.pop(0)
# ys.pop(0)
# zs.pop(0)

index = 0
for x in xs:
	teleport_x = x + 0.5
	teleport_z = zs[index] + 0.5
	teleport_y = ys[index]
	tp_command = "tp " + str(teleport_x) + " " + \
		str(teleport_y)+" " + str(teleport_z)
	print(str(index) + ": "+"Sending command: " + tp_command)
	agent_host.sendCommand(tp_command)

	agent_host.sendCommand("attack 1")
	time.sleep(3)
	index += 1

# Visited all the locations - quit the mission.
agent_host.sendCommand("quit")
while world_state.is_mission_running:
	world_state = agent_host.peekWorldState()

print("Teleport mission over.")
world_state = agent_host.getWorldState()
if world_state.number_of_rewards_since_last_state > 0:
	total_reward += world_state.rewards[-1].getValue()
if total_reward != 100:
	print("Got incorrect reward (" + str(total_reward) +
		  ") - should have received 100 for collecting 100 emeralds.")
	exit(1)

print("Test successful")
exit(0)