from __future__ import print_function
from builtins import range
import MalmoPython
import os
import sys
import time
import random
import json
import math

if sys.version_info[0] == 2:
    # flush print output immediately
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
else:
    import functools
    print = functools.partial(print, flush=True)

# Create default Malmo objects:
agent_host = MalmoPython.AgentHost()
try:
    agent_host.parse(sys.argv)
except RuntimeError as e:
    print('ERROR:', e)
    print(agent_host.getUsage())
    exit(1)
if agent_host.receivedArgument("help"):
    print(agent_host.getUsage())
    exit(0)

# -- set up the mission -- #
missionXML_file = 'nb4tf4i_d.xml'
with open(missionXML_file, 'r') as f:
    mission_xml = f.read()
    my_mission = MalmoPython.MissionSpec(mission_xml, True)
    my_mission.drawBlock(0, 0, 0, "lava")


obRng = 3
forward = obRng*(int(obRng/2)+1)-1 + obRng*obRng * \
    int(obRng/2) - (int(obRng/2)-1)
left = int(obRng/2) + obRng*obRng*int(obRng/2) + (int(obRng/2)-1)*obRng
right = obRng*obRng - 1 - (int(obRng/2)+(int(obRng/2)-1)
                           * obRng) + obRng*obRng*int(obRng/2)
backward = obRng*int(obRng/2) + obRng*obRng*int(obRng/2) + (int(obRng/2)-1)


class Steve:
    def __init__(self, agent_host):
        self.agent_host = agent_host

        self.x = 0
        self.y = 0
        self.z = 0

        self.corners = 0

        self.smallDelay = .3

        self.hasFlower = {}
        for i in range(64):
            self.hasFlower[i] = True

    def run(self):
        world_state = self.agent_host.getWorldState()

        self.agent_host.sendCommand("look 1")
        self.agent_host.sendCommand("look 1")
        self.agent_host.sendCommand("turn -1")
        self.agent_host.sendCommand("turn -1")

        while not world_state.is_mission_running:
            time.sleep(.1)
            world_state = self.agent_host.getWorldState()

        while world_state.number_of_observations_since_last_state == 0:
            time.sleep(.1)
            world_state = self.agent_host.getWorldState()

        input = world_state.observations[-1].text
        observations = json.loads(input)
        nbr = observations.get("nbr3x3", 0)

        # go to ledge
        while nbr[forward] != "dirt":
            print(nbr[forward])
            self.agent_host.sendCommand("move 1")
            time.sleep(self.smallDelay)
            print("going to ledge")

            input = world_state.observations[-1].text
            observations = json.loads(input)
            nbr = observations.get("nbr3x3", 0)

            world_state = self.agent_host.getWorldState()

        # Main Loop
        while world_state.is_mission_running:
            if world_state.number_of_observations_since_last_state != 0:

                input = world_state.observations[-1].text
                observations = json.loads(input)
                nbr = observations.get("nbr3x3", 0)

                # update location
                self.x = observations["XPos"]
                self.y = observations["YPos"]
                self.z = observations["ZPos"]

                print(self.corners)

                # go up a level if we've gone around or our level has no more flowers
                if self.corners > 4 or (self.hasFlower[int(self.y)] == False):
                    print("moving up")
                    self.goUp(nbr)
                    self.corners = 0

                # find flower in grid
                flowerCoords = self.findFlower(nbr)
                if flowerCoords:
                    # go to and mine flower
                    print(flowerCoords)
                    self.gotoFlower(flowerCoords, nbr)
                else:
                    # go
                    if nbr[forward] == "dirt":  # előttünk
                        if nbr[right] == "dirt":  # jobbra
                            self.agent_host.sendCommand("strafe -1")
                            self.corners += 1
                        elif nbr[left] == "dirt":  # balra
                            self.agent_host.sendCommand("move -1")
                            self.corners += 1
                        else:
                            self.agent_host.sendCommand("strafe -1")
                    elif nbr[backward] == "dirt":  # mögöttünk
                        if nbr[left] == "dirt":  # balra
                            self.agent_host.sendCommand("strafe 1")
                            self.corners += 1
                        elif nbr[right] == "dirt":  # jobbra
                            self.agent_host.sendCommand("move 1")
                            self.corners += 1
                        else:
                            self.agent_host.sendCommand("strafe 1")
                    else:
                        if nbr[left] == "dirt":  # balra
                            self.agent_host.sendCommand("move -1")
                        elif nbr[right] == "dirt":  # jobbra
                            self.agent_host.sendCommand("move 1")
                        else:
                            self.agent_host.sendCommand("move 1")

                time.sleep(self.smallDelay)

                world_state = self.agent_host.getWorldState()

    def findFlower(self, nbrs):

        for i in range(obRng*obRng*obRng):
            if nbrs[i] == "red_flower":
                x = (i % (obRng))-int(obRng/2)
                y = i//(obRng*obRng)-int(obRng/2)
                z = i//(obRng)-obRng * (i//(obRng*obRng))-int(obRng/2)

                if y < 1 and abs(x) < 2 and abs(z) < 2:
                    return (x, y, z)
        return None

    def findHotKeyForBlockType(self, ob, type):
        '''Hunt in the inventory hotbar observations for the slot which contains the requested type.'''
        for i in range(0, 9):
            slot_name = u'Hotbar_' + str(i) + '_item'
            slot_contents = ob.get(slot_name, "")
            if slot_contents == type:
                # +1 to convert from 0-based inventory slot to 1-based hotbar key.
                return i+1
        return -1

    def gotoFlower(self, coordsRelative, nbrs):

        # APPROACH HOSTILES
        self.agent_host.sendCommand("move "+str(coordsRelative[0]))
        time.sleep(self.smallDelay)
        self.agent_host.sendCommand("strafe "+str(coordsRelative[2]))
        time.sleep(self.smallDelay)

        # DESTROY LIBERALS
        self.agent_host.sendCommand("attack 1")
        time.sleep(self.smallDelay)
        self.agent_host.sendCommand("attack 0")
        time.sleep(self.smallDelay)

        # Update observations
        world_state = self.agent_host.getWorldState()

        input = world_state.observations[-1].text
        observations = json.loads(input)
        nbr = observations.get("nbr3x3", 0)

        time.sleep(self.smallDelay)

        # Find hotbar with dirt
        asd = str(self.findHotKeyForBlockType(observations, "dirt"))

        self.agent_host.sendCommand("hotbar."+asd+" 1")
        self.agent_host.sendCommand("hotbar."+asd+" 0")

        # CONSTRUCT POLICE STATE
        while nbr[forward] == "dirt" and nbr[left] == "dirt" and nbr[backward] == "dirt" and nbr[right] == "dirt":
            self.agent_host.sendCommand("jump 1")
            self.agent_host.sendCommand("jump 1")
            time.sleep(.1)  # WHYYYYYYYYYYYYYYY
            self.agent_host.sendCommand("use")
            time.sleep(self.smallDelay)

            world_state = self.agent_host.getWorldState()

            input = world_state.observations[-1].text
            observations = json.loads(input)
            nbr = observations.get("nbr3x3", 0)

            time.sleep(self.smallDelay)

        self.hasFlower[int(self.y)] = False

    def goUp(self, nbrs):
        turns = 0

        if nbrs[forward] == "dirt":
            pass
        elif nbrs[right] == "dirt":
            turns = 1
        elif nbrs[backward] == "dirt":
            turns = 2
        elif nbrs[left] == "dirt":
            turns = 3

        for i in range(turns):
            self.agent_host.sendCommand("turn 1")
            time.sleep(self.smallDelay)

        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(self.smallDelay)

        for i in range(turns):
            self.agent_host.sendCommand("turn -1")
            time.sleep(self.smallDelay)

# try to start mission
num_repeats = 1
for ii in range(num_repeats):

    my_mission_record = MalmoPython.MissionRecordSpec()

    # Attempt to start a mission:
    max_retries = 6
    for retry in range(max_retries):
        try:
            agent_host.startMission(my_mission, my_mission_record)
            break
        except RuntimeError as e:
            if retry == max_retries - 1:
                print("Error starting mission:", e)
                exit(1)
            else:
                print("Attempting to start the mission:")
                time.sleep(2)

    # Loop until mission starts:
    print("   Waiting for the mission to start")
    world_state = agent_host.getWorldState()

    while not world_state.has_mission_begun:
        time.sleep(0.15)
        world_state = agent_host.getWorldState()
        for error in world_state.errors:
            print("Error:", error.text)

    print("RFH running\n")
    steve = Steve(agent_host)
    steve.run()
    time.sleep(1)
